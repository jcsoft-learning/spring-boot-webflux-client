package com.jcsoft.springbootwebfluxclient;

import com.jcsoft.springbootwebfluxclient.handler.ProductHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration
public class RouterConfig
{
    @Bean
    public RouterFunction<ServerResponse> routes(final ProductHandler productHandler)
    {
        return RouterFunctions.route(GET("/api/client"), productHandler::list)
                .andRoute(GET("/api/client/{id}"), productHandler::show)
                .andRoute(POST("/api/client"), productHandler::create)
                .andRoute(PUT("/api/client/{id}"), productHandler::edit)
                .andRoute(DELETE("/api/client/{id}"), productHandler::delete)
                .andRoute(POST("/api/client/upload/{id}"), productHandler::upload);
    }
}
