package com.jcsoft.springbootwebfluxclient.service;

import com.jcsoft.springbootwebfluxclient.model.Product;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;

/**
 * Es preferible utilizar retrieve ya que devuelve una excepción si algo falla del server si es que existe, utilizando
 * exchange es cuando se desea hacer más modificaciones.
 */
@Service
public class ProductServiceImpl implements ProductService
{
    private WebClient.Builder webClientBuilder;

    public ProductServiceImpl(final WebClient.Builder webClientBuilder)
    {
        this.webClientBuilder = webClientBuilder;
    }

    @Override
    public Flux<Product> findAll()
    {
        return webClientBuilder.build().get().accept(MediaType.APPLICATION_JSON)
                .exchangeToFlux(clientResponse -> clientResponse.bodyToFlux(Product.class));
    }

    @Override
    public Mono<Product> findById(final String id)
    {
        return webClientBuilder.build().get().uri("/{id}", Collections.singletonMap("id", id))
                .accept(MediaType.APPLICATION_JSON)
                //.exchangeToMono(clientResponse -> clientResponse.bodyToMono(Product.class));
                .retrieve()
                .bodyToMono(Product.class);
    }

    @Override
    public Mono<Product> save(final Product product)
    {
        return webClientBuilder.build().post()
                .accept(MediaType.APPLICATION_JSON) //contenido aceptado en el response
                .contentType(MediaType.APPLICATION_JSON) //contenido enviado
                .body(BodyInserters.fromValue(product))
                //.bodyValue(product)
                .retrieve()
                .bodyToMono(Product.class);
    }

    @Override
    public Mono<Product> update(final Product product, final String id)
    {
        return webClientBuilder.build().put()
                .uri("/{id}", Collections.singletonMap("id", id))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(product)
                .retrieve()
                .bodyToMono(Product.class);
    }

    @Override
    public Mono<Void> delete(final String id)
    {
        return webClientBuilder.build().delete()
                .uri("/{id}", Collections.singletonMap("id", id))
                .retrieve()
                .bodyToMono(Void.class)
                ;
    }

    @Override
    public Mono<Product> upload(final FilePart filePart,
                                final String id)
    {
        MultipartBodyBuilder parts = new MultipartBodyBuilder();
        parts.asyncPart("file", filePart.content(), DataBuffer.class).headers(httpHeaders -> {
            httpHeaders.setContentDispositionFormData("file", filePart.filename());
        });
        return webClientBuilder.build().post().uri("/upload/{id}", Collections.singletonMap("id", id))
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .bodyValue(parts.build())
                .retrieve()
                .bodyToMono(Product.class);
    }
}
