package com.jcsoft.springbootwebfluxclient.handler;

import com.jcsoft.springbootwebfluxclient.model.Product;
import com.jcsoft.springbootwebfluxclient.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
public class ProductHandler
{
    private final ProductService productService;

    public ProductHandler(final ProductService productService)
    {
        this.productService = productService;
    }

    public Mono<ServerResponse> list(final ServerRequest serverRequest)
    {
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                .body(productService.findAll(), Product.class);
    }

    public Mono<ServerResponse> show(final ServerRequest serverRequest)
    {
        final String id = serverRequest.pathVariable("id");
        return errorHandler(productService.findById(id).flatMap(
                product -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(product)
                        .switchIfEmpty(ServerResponse.notFound().build()))
        );
    }

    public Mono<ServerResponse> create(final ServerRequest serverRequest)
    {
        Mono<Product> productMono = serverRequest.bodyToMono(Product.class);

        return productMono.flatMap(product -> {
            if (Objects.isNull(product.getCreateAt())) {
                product.setCreateAt(LocalDate.now());
            }
            return productService.save(product).flatMap(
                    // Aquí estamos construyendo el cuerpo del response a partir de la respuesta del request (al server)
                    productSv -> ServerResponse.created(URI.create("/api/client".concat(productSv.getId())))
                            .contentType(MediaType.APPLICATION_JSON)
                            .bodyValue(productSv))
                    .onErrorResume(throwable -> {
                        final WebClientResponseException errorResponse = (WebClientResponseException) throwable;
                        // Mapeamos el posible error del server para mostrarlo en el client del client tal cual como
                        // devuelve el server ya que si no lo mapeamos aqui dará un error 500 (ejemplo intentar crear
                        // con un JSON vacio).
                        if (errorResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
                            return ServerResponse.badRequest()
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .bodyValue(errorResponse.getResponseBodyAsString());
                        }
                        return Mono.error(errorResponse);
                    });
        });
    }

    public Mono<ServerResponse> edit(final ServerRequest serverRequest)
    {
        Mono<Product> productMono = serverRequest.bodyToMono(Product.class);
        final String id = serverRequest.pathVariable("id");

        // Para poder manejar errores del server debemos extraer el update de productService ya que el error sucede ahí
        /*return productMono.flatMap(
                product -> ServerResponse.created(URI.create("/api/client".concat(id)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(productService.update(product, id), Product.class)
        );*/
        return productMono.flatMap(product -> productService.update(product, id))
                .flatMap(product -> ServerResponse.created(URI.create("/api/client".concat(id)))
                        .contentType(MediaType.APPLICATION_JSON)
                        // Tenemos que usar bodyValue ya que product es syncrono, no es un publisher como arribita en que
                        // el product venía desde el service que a su vez traía el dato del server asyncronamente.
                        .bodyValue(product))
                .onErrorResume(throwable -> {
                    final WebClientResponseException errorResponse = (WebClientResponseException) throwable;
                    if (errorResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                        return ServerResponse.notFound().build();
                    }
                    return Mono.error(errorResponse);
                });
    }

    public Mono<ServerResponse> delete(final ServerRequest serverRequest)
    {
        final String id = serverRequest.pathVariable("id");

        return productService.delete(id).then(ServerResponse.noContent().build())
                .onErrorResume(throwable -> {
                    final WebClientResponseException errorResponse = (WebClientResponseException) throwable;
                    if (errorResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                        return ServerResponse.notFound().build();
                    }
                    return Mono.error(errorResponse);
                });
    }

    public Mono<ServerResponse> upload(ServerRequest serverRequest)
    {
        String id = serverRequest.pathVariable("id");
        return serverRequest.multipartData().map(multipart -> multipart.toSingleValueMap().get("file"))
                .cast(FilePart.class)
                .flatMap(part -> productService.upload(part, id))
                .flatMap(product -> ServerResponse.created(URI.create("/api/client".concat(id)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(product))
                .onErrorResume(throwable -> {
                    final WebClientResponseException errorResponse = (WebClientResponseException) throwable;
                    if (errorResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                        return ServerResponse.notFound().build();
                    }
                    return Mono.error(errorResponse);
                });
    }

    /**
     * Una forma de manejar los errores para no repetir codigo, usarlo en todos los que mapeen NOT_FOUND
     * @param serverResponse
     * @return
     */
    private Mono<ServerResponse> errorHandler(Mono<ServerResponse> serverResponse) {
        return serverResponse.onErrorResume(throwable -> {
            final WebClientResponseException errorResponse = (WebClientResponseException) throwable;
            if (errorResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                // si queremos devolver un cuerpo con el NOT_FOUND:
                Map<String, Object> body = new HashMap<>();
                body.put("error", "No existe el producto: ".concat(
                        Objects.requireNonNull(errorResponse.getMessage())));
                body.put("timestamp", LocalDateTime.now());
                body.put("status", errorResponse.getStatusCode().value());
                return ServerResponse.status(HttpStatus.NOT_FOUND).bodyValue(body);
            }
            return Mono.error(errorResponse);
        });
    }
}
